FROM node

# Create app directory
WORKDIR /usr/src/app

# Install node dependencies
COPY package.json .
RUN npm install

# Add app files
COPY . .

EXPOSE 8000