import express from 'express';
import bodyParser from 'body-parser';

const app = express();
const port = process.env.PORT || 8000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', (req, res) => {
    res.send('Welcome to my API inside Docker!');
});

app.listen(port, () => {
    console.log(`Running on port ${port}`);
});